import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
public class Type{
	
	
    public int WType(String[] compw){
    	
        if(compw.length==1)
        {
            if(IsGradable(compw[0]))
              //  System.out.println("type 1");
           return 1;
            else if(IsIrregularComparator(compw[0]))
 //               System.out.println("type 1");
            return 1;
            else if(RealType1(compw[0]))
//                System.out.println("type 1");
            return 1;
            
        }
       
            return 2;
//    return 2;
    }

    public static boolean RealType1(String word){
        int l=word.length();
        if(l>=2 && word.charAt(l-2)=='e' && word.charAt(l-1)=='r')
            return true;
        else if(l>=3 && word.charAt(l-3)=='e' && word.charAt(l-2)=='s' && word.charAt(l-1)=='t')
            return true;
        return false;
    }

    public static boolean IsGradable(String word) {
	BufferedReader br = null;
//        int flag =0;
	try {
	    String sCurrentLine;
	    br = new BufferedReader(new FileReader("resources\\gradable-words.txt"));
	    while ((sCurrentLine = br.readLine()) != null) {
                if(sCurrentLine.equals(word)==true){
//                    System.out.println("its gradable");
                    return true;
//                    flag=1;
                }
	    }
//            if(flag==0){
//                System.out.println("can't guarantee");
                return false;
//            }
 
	    } catch (IOException e) {
		e.printStackTrace();
	    } finally {
		try {
		    if (br != null)br.close();
		    } catch (IOException ex) {
		    ex.printStackTrace();
		    }
	    }
        return false; 
    }

    public static boolean IsIrregularComparator(String word) {
	BufferedReader br = null;
//        int flag =0;
	try {
	    String sCurrentLine;
	    br = new BufferedReader(new FileReader("resources\\irregular-words.txt"));
	    while ((sCurrentLine = br.readLine()) != null) {
                if(sCurrentLine.equals(word)==true){
//                    System.out.println("its irregualar");
                    return true;
//                    flag=1;
                }
	    }
//            if(flag==0){
//                System.out.println("can't guarantee");
                return false;
//            }
 
	    } catch (IOException e) {
		e.printStackTrace();
	    } finally {
		try {
		    if (br != null)br.close();
		    } catch (IOException ex) {
		    ex.printStackTrace();
		    }
	    }
        return false;
    }
}
