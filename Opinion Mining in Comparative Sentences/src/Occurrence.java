import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashSet;
import java.util.StringTokenizer;


public class Occurrence {
	
	private String feature;
	private String entity1;
	private String entity2;
	OSA osa=new OSA();
	

	boolean IsFeatureIndicator(String compw){
		if(!entity1.isEmpty() && !entity2.isEmpty()){
			if(feature.isEmpty()){
				return true;				
			}	
		}
			
		return false;
	}
	
	public int OC_pros(String compw){
		int occur = 0;
		
		try {
			//Could be done Globally
			BufferedReader brPros = new BufferedReader(new FileReader("resources\\IntegratedPros.txt"));
			//BufferedReader brCons = new BufferedReader(new FileReader("C:\Users\Akshay Tayal\Desktop\ire\MajorProject\proscons\IntegratedCons.txt"));
			
			//we can make these variables as shared with OC_cons as this is redundant code
			//Also, getSynonyms will be implemented by Rashmi
			HashSet<String> compwSynonyms = osa.getSynonyms(compw);
			if(compw.equalsIgnoreCase("heavier"))System.out.println(compwSynonyms);
			
			String line;
			StringTokenizer st;
			
			while((line=brPros.readLine())!=null) {
				
				st = new StringTokenizer(line);
				while(st.hasMoreTokens()) {
					String l=st.nextToken();
					if(compwSynonyms.contains(l)) {
						occur++;
					}			
				}	
			}
			//Make global if the above is global
			brPros.close();
			//brCons.close();
			
		} catch (Exception e) {			
			e.printStackTrace();
		}
		
		return occur;
	}
	
	public int OC_cons(String compw){
		
		int occur = 0;
		
		try {
			//Could be done Globally
			BufferedReader brCons = new BufferedReader(new FileReader("resources\\IntegratedCons.txt"));
			//Also, getSynonyms will be implemented by Rashmi
			HashSet<String> compwSynonyms = osa.getSynonyms(compw);
			
			//System.out.println(compwSynonyms);
			if(compw.equalsIgnoreCase("heavier"))System.out.println(compwSynonyms);
			String line;
			StringTokenizer st;			
			
			while((line=brCons.readLine())!=null) {
				
				st = new StringTokenizer(line);
				while(st.hasMoreTokens()) {
					String l=st.nextToken();
					if(compwSynonyms.contains(l)) {
						occur++;
					}					
				}
			}
			//Make global if the above is global
			brCons.close();
			
		} catch (Exception e) {			
			e.printStackTrace();
		}
		
		return occur;		
		
	}
	
	

}
