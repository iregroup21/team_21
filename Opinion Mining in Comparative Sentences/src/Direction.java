import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

//import edu.upc.freeling.*;
public class Direction {
	
	Map<String,Boolean> increasing;
	Map<String,Boolean> decreasing;
	
	
	public Direction()
	{
		increasing=new HashMap<String,Boolean>();
		decreasing=new HashMap<String,Boolean>();
		WordNet wn=new WordNet();
		
		BufferedReader inc,dec;
		
		try {
			inc=new BufferedReader(new FileReader("resources\\increasing.txt"));
			dec=new BufferedReader(new FileReader("resources\\decreasing.txt"));
			
			String word;
			
			while((word=inc.readLine())!=null)	
			{
				increasing.put(word, true);
				
				ArrayList<String> synonyms=wn.getSyns(word);	
				
				
				int len=synonyms.size();
				
				for(int i=0;i<len;i++)
					increasing.put(synonyms.get(i), true);
				
			}
			
			while((word=dec.readLine())!=null)
			{
				decreasing.put(word, true);
				
				ArrayList<String> synonyms=wn.getSyns(word);
				int len=synonyms.size();
				
				for(int i=0;i<len;i++)
					decreasing.put(synonyms.get(i), true);
			}
			
			
			inc.close();
			dec.close();
						
		} catch (IOException e) {			
			e.printStackTrace();
		}
	}
	
	public String getDirection(String word)
	{
		if(increasing.containsKey(word))return "inc";
		else if(decreasing.containsKey(word))return "dec";
		
		return "none";
	}
	
	public boolean IsIncreasing(String word)
	{
		if(increasing.containsKey(word))return true;
		return false;
	}
	
	public boolean IsDecreasing(String word)
	{
		if(decreasing.containsKey(word))return true;
		return false;
	}

}
