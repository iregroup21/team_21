/*  Takes a command line argument word and prints if its opinionated or not  */
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class IsOpinionated{
	
	BaseForm bsf;
	WordNet wn;
	
	public IsOpinionated()
	{
		bsf=new BaseForm();
		wn=new WordNet();
	}
	
    public boolean IsCWOpinionated(String word){
    	
    	//System.out.println("Now checking (IsCWOpinionated)"+word);
    	
    	//System.out.println();
    	    	
    	if(IsPositive(word)==true || IsNegative(word)==true)
            return true;
    	
    	
    	/*for(String w : syns){
        if(IsPositive(w)==true || IsNegative(w)==true)
            return true;
    	}*/
            return false;
    }
    
    
    public boolean IsNegative(String word) {
	BufferedReader br = null;
	ArrayList<String> syns = wn.getSyns(word);
	
	try {
	    String sCurrentLine;
	    br = new BufferedReader(new FileReader("resources\\negative-words.txt"));
	    while ((sCurrentLine = br.readLine()) != null) {
	    		    	
	    		for(String w : syns){
	    		//	System.out.println("Checking "+w);
                if(sCurrentLine.equalsIgnoreCase(word)==true)// || sCurrentLine.equalsIgnoreCase(w))
                        return true;}
	    }
	    } catch (IOException e) {
		    e.printStackTrace();
		} finally {
			try {
				if (br != null)br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
                return false;
	}
    
    
    public boolean IsPositive(String word) {
	BufferedReader br = null;
	ArrayList<String> syns = wn.getSyns(word);
	//System.out.println(syns);
	try {
	    String sCurrentLine;
	    br = new BufferedReader(new FileReader("resources\\positive-words.txt"));
	    while ((sCurrentLine = br.readLine()) != null) {
	    	
	    	for(String w : syns){
	    		//System.out.println("Checking "+w);
                if(sCurrentLine.equalsIgnoreCase(word)==true)// || sCurrentLine.equalsIgnoreCase(w))
                        return true;}
	    }
	    } catch (IOException e) {
		    e.printStackTrace();
		} finally {
			try {
				if (br != null)br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
                return false;
	}
}
