import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;


public class Parser {
	
	BufferedReader fbr;
	
	public Parser(String file)
	{
		try {
			//fbr=new BufferedReader(new FileReader("labeledSentences.txt"));
			fbr=new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public HashMap<String,String[]> Parse()
	{
		
			StringBuffer comp=new StringBuffer();
			String line;
			boolean isline=false;
			boolean isbreak=false;
			String sentence=null;
			HashMap<String,String[]> mapping=new HashMap<String, String[]>();
			
			try {
			
			while((line=fbr.readLine())!=null)
			{
				line=line.trim();
				//if(line.compareTo("<cs-1>")==0 || line.compareTo("<cs-3>")==0){isline=true;}// System.out.println("in");}
				//else if(line.compareTo("</cs-1>")==0 || line.compareTo("</cs-3>")==0){isbreak=true;}// System.out.println("in");}
				
				if(line.startsWith("<cs-1>") || line.startsWith("<cs-3>")){isline=true;}// System.out.println("in");}
				else if(line.startsWith("</cs-1>") || line.startsWith("</cs-3>")){isbreak=true;}// System.out.println("in");}
				
				else if(isline){sentence=line; isline=false; }
				else if(isbreak) {
					String lin=line.trim();
					
					if(lin.length()>1)
					mapping.put(sentence, lin.split(" +")); 
					sentence=null; isbreak=false;
					 
					}				
			}
			
			fbr.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		return mapping;
	}
	
	

}
