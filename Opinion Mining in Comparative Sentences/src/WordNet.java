import java.util.ArrayList;

import edu.smu.tspell.wordnet.Synset;
import edu.smu.tspell.wordnet.WordNetDatabase;

//import edu.upc.freeling.*;

public class WordNet {
	
	public ArrayList<String> getSyns(String word)	// Return list of synonyms of a word
	{
		WordNetDatabase database = WordNetDatabase.getFileInstance();
		Synset[] synsets = database.getSynsets(word);
		ArrayList<String> synonyms=new ArrayList<String>();
		
		
		if (synsets.length > 0)
		{

			for (int i = 0; i < synsets.length; i++)
			{
				String[] wordForms = synsets[i].getWordForms();
			
				for (int j = 0; j < wordForms.length; j++)
				{
					synonyms.add(wordForms[j]);
				}
			}
		}
		
		return synonyms;
	}

}
