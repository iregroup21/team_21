import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class SystemEval {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.setProperty("wordnet.database.dir", "C:\\Program Files (x86)\\WordNet\\2.1\\dict");
		SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
		XMLHandler xhandler = new XMLHandler();
		float percentage , precision , recall , fmeasure;
		
		 try {
			SAXParser saxParser = saxParserFactory.newSAXParser();
			//saxParser.parse(new File("filter1_1300.xml"), xhandler);
			
			File file=new File("SPA_modified.xml");
			InputStream ist=new FileInputStream(file);
			Reader reader=new InputStreamReader(ist);
			InputSource is=new InputSource(reader);
			is.setEncoding("UTF-8");
						
			
			saxParser.parse(is, xhandler);
			
			percentage=(xhandler.correct*100)/xhandler.total;
			
		//	System.out.println(xhandler.correctpositive + "  " + xhandler.allreturnedpositive + " " + xhandler.allpositive);
			precision=(float)xhandler.correctpositive / xhandler.allreturnedpositive;
			recall=(float)xhandler.correctpositive / xhandler.allpositive;
			fmeasure= (2 * precision * recall) / (precision + recall);
			
			System.out.println("P:"+precision+" "+"R:"+recall);
			//System.out.println("Accuracy:"+ percentage +"% (" + xhandler.correct + "/" + xhandler.total + ")");
			//System.out.println("Non2B incorrect:"+xhandler.non2b);
			System.out.println("F-measure:"+ fmeasure);
			
			
		} catch (ParserConfigurationException | SAXException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
	}

}
