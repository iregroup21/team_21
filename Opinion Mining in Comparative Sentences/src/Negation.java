import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;


public class Negation {

	/**
	* @param args
	* @throws IOException 
	*/
	
	public static boolean CheckNegativeComparator(String sentence, String compw) throws IOException{
		StringTokenizer st;
		int i, flag =0;
		int start=0, compPos=0;
		List<String> senList  = new LinkedList<String>();
		List<String> negList = new LinkedList<String>();
		List<String> negNullify = new LinkedList<String>();
		
		negNullify.add("just");
		negNullify.add("only");
		negNullify.add("yet");
		
		BufferedReader negWords = new BufferedReader(new FileReader("resources\\negwords.txt"));
		String line;
		
		while((line=negWords.readLine())!=null) {
			negList.add(line.trim());
		}
		
		
		st = new StringTokenizer(sentence);
		while(st.hasMoreTokens()) {
			String l=st.nextToken();
			senList.add(l);
				
			}
		for(compPos=0; compPos<5; compPos++){
			if(senList.get(compPos).equalsIgnoreCase(compw)){
				flag=1;
				break;
				
			}
		}
		
		if(flag != 1){
			compPos++;
			for(; compPos < senList.size(); compPos++){
				if(senList.get(compPos).equalsIgnoreCase(compw)){
					flag=1;
					break;
					
				}
				start++;
				
			}
		}
		
		//System.out.println("Here");
		if(flag==0){
			System.out.print("");
			///Do Nothing
		}
		else{
			i = start;
			while(i<compPos){
				if( negList.contains(senList.get(i)) && !negNullify.contains(senList.get(i+1))){
					return true;
				}
				i++;
			}
		}
		
		return false;
		
	}
}