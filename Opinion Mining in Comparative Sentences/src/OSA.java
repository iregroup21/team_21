import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.*;

import edu.smu.tspell.wordnet.Synset;
import edu.smu.tspell.wordnet.WordNetDatabase;
import edu.smu.tspell.wordnet.WordSense;

public class OSA {
	
	static WordNet wn=new WordNet();
	
	public static void main(String args[]) {
		
		String line="X is better than y";		
		Boolean chk = NegationIsPresent(line);
		System.out.println(chk);
		
		String feature="",compw="";
		
		OSA osa=new OSA();
		osa.OSAPref(args[0], args[1]);
		
		
	}

	public int OSAPref(String feature, String compword) {
		
	//	BaseForm bsf=new BaseForm();
	//	String[] baseforms=bsf.getBaseForm(compword);
		Double osaP=0.0,osaN=0.0;
	//	int len=baseforms.length;
		
		
		
		
	//	for(String compw : baseforms){
		osaP += OSA_Positive(feature, compword);
		osaN += OSA_Negative(feature, compword);
	//	}
		
	//	System.out.println(feature+":"+osaP+","+osaN);
		
		if(Double.isNaN(osaP) || Double.isNaN(osaN))return 1;
		
		if(osaN==Double.NEGATIVE_INFINITY)return 1;
		else if(osaP==Double.NEGATIVE_INFINITY)return 2;
		
		
		if(osaP>=osaN)
			return 1;
		else
			return 2;
		
		
	}
	//Changed d name from OSA_pros to OSA_Positive
	public Double OSA_Positive(String feature, String compw) {
		
		Double countFC = 0.0,prF = 0.0,prC = 0.0,prCF = 0.0,osa = 0.0,prFC=0.0;
		boolean flagC=false,flagF=false;
		
		try {
			//Could be done Globally
			BufferedReader brPros = new BufferedReader(new FileReader("resources\\IntegratedPros.txt"));
			BufferedReader brCons = new BufferedReader(new FileReader("resources\\IntegratedCons.txt"));
			
			HashSet<String> compwSynonyms = getSynonyms(compw);
			HashSet<String> compwAntonyms = getAntonyms(compw);
			HashSet<String> featureSynonyms = getSynonyms(feature);
			
			String line;
			StringTokenizer st;
			
			while((line=brPros.readLine())!=null) {
				
				st = new StringTokenizer(line);
				while(st.hasMoreTokens()) {
					String l=st.nextToken();
					if(compwSynonyms.contains(l)) {
						prC++;						
						flagC=true;
						
					}
					else if(featureSynonyms.contains(l)) {
						prF++;
						if(flagC)
							countFC++;
					
						flagC=false;
					}
					else
						flagC=false;
						
				}
				
				if(line.endsWith("</Pros>")) {
				/*	if(flagC && flagF)
						countFC++;
					flagC=false;
					flagF=false;
				*/	
				}
				
				
			}
			
			
			while((line=brCons.readLine())!=null) {
				
				st = new StringTokenizer(line);
				while(st.hasMoreTokens()) {
					String l=st.nextToken();
					if(compwAntonyms.contains(l)) {
						prC++;
						flagC=true;
					}
					else if(featureSynonyms.contains(l)) {
						prF++;
						if(flagC)
							countFC++;
					
						flagC=false;
					}
					else
						flagC=false;
						
				}
				
				if(line.endsWith("</Pros>")) {
				/*	if(flagC && flagF)
						countFC++;
					flagC=false;
					flagF=false;
				*/	
				}				
			}
			
			prFC=(countFC/(prF+prC));
			prCF=(countFC/prF);
			
		//	System.out.println("||||||"+prF+"  "+prC+"|||||||");
		//	System.out.println("||||||"+prFC+"  "+prCF+"|||||||");
			
			osa = Math.log((prFC*prCF)/(prF*prC));
			
			
			
			
			//Make global if the above is global
			brPros.close();
			brCons.close();
			
		} catch (Exception e) {			
			e.printStackTrace();
		}
		
		return osa;
	}

	

	public static HashSet<String> getSynonyms(String word) {
		// TODO 
		/*
		 1: Dont forget to put the word in hashset
		 2: find synonmys from wordnet and add them to hashset
		 */
		
		HashSet<String> syn = new HashSet<>();
		ArrayList<String> synonyms=wn.getSyns(word);	
		
		
		int len=synonyms.size();
		
		for(int i=0;i<len;i++)
			syn.add(synonyms.get(i));
		
		return syn;
	}

	public static HashSet<String> getAntonyms(String word) {
		// TODO 
				/*
				 1: Dont forget to put the word in hashset
				 2: find synonyms from wordnet and add them to hashset
				 */
		
		WordNetDatabase database = WordNetDatabase.getFileInstance();
		
		Synset[] synsets = database.getSynsets(word);
		
		HashSet<String> antonyms=new HashSet<String>();
		
		for(int i=0;i<synsets.length;i++)
		{
			WordSense ws[]=synsets[i].getAntonyms(word);
			
			
			for(int j=0;j<ws.length;j++)
			{
				antonyms.add(ws[j].getWordForm());
			}
			
		}
		
		return antonyms;
	}

	private static Double OSA_Negative(String feature, String compw) {

		//Only change made from OSA_Positive is exchange the position of compwAntonyms and compwSyonyms
		Double countFC = 0.0,prF = 0.0,prC = 0.0,prCF = 0.0,osa = 0.0,prFC=0.0;
		boolean flagC=false,flagF=false;
		
		try {
			//Could be done Globally
			BufferedReader brPros = new BufferedReader(new FileReader("resources\\IntegratedPros.txt"));
			BufferedReader brCons = new BufferedReader(new FileReader("resources\\IntegratedCons.txt"));
			
			HashSet<String> compwSynonyms = getSynonyms(compw);
			HashSet<String> compwAntonyms = getAntonyms(compw);
			HashSet<String> featureSynonyms = getSynonyms(feature);
			
			String line;
			StringTokenizer st;
			
			while((line=brPros.readLine())!=null) {
				
				st = new StringTokenizer(line);
				while(st.hasMoreTokens()) {
					String l=st.nextToken();
					if(compwAntonyms.contains(l)) {
						prC++;
						flagC=true;
					}
					else if(featureSynonyms.contains(l)) {
						prF++;
						if(flagC)
							countFC++;
					
						flagC=false;
					}
					else
						flagC=false;
						
				}
				
				if(line.endsWith("</Pros>")) {
				/*	if(flagC && flagF)
						countFC++;
					flagC=false;
					flagF=false;
				*/	
				}
				
			}
			
			
			while((line=brCons.readLine())!=null) {
				
				st = new StringTokenizer(line);
				while(st.hasMoreTokens()) {
					String l=st.nextToken();
					if(compwSynonyms.contains(l)) {
						prC++;
						flagC=true;
					}
					else if(featureSynonyms.contains(l)) {
						prF++;
						if(flagC)
							countFC++;
					
						flagC=false;
					}
					else
						flagC=false;
						
				}
				
				if(line.endsWith("</Pros>")) {
				/*	if(flagC && flagF)
						countFC++;
					flagC=false;
					flagF=false;
				*/	
				}
				
				
			}
			
			prFC=(countFC/(prF+prC));
			prCF=(countFC/prF);
			osa = Math.log((prFC*prCF)/(prF*prC));
			
			
			
			//Make global if the above is global
			brPros.close();
			brCons.close();
			
		} catch (Exception e) {			
			e.printStackTrace();
		}
		
		return osa;

	}

	private static Boolean NegationIsPresent(String line) {
		
		//Preprocess this in code and make negativeWords global
		HashSet<String> negativeWords= new HashSet<>(Arrays.asList("no","not","never","nobody",
				"none","neither","nor"));
		
		StringTokenizer st = new StringTokenizer(line);
		while(st.hasMoreTokens()) {
			String l=st.nextToken();
			if(negativeWords.contains(l) || l.contains("n't")) {
				return true;
			}
			
		}		
		return false;
	}

}
