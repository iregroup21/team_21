import edu.smu.tspell.wordnet.SynsetType;
import edu.smu.tspell.wordnet.WordNetDatabase;
import edu.smu.tspell.wordnet.impl.file.Morphology;

public class BaseForm {
	
	public String[] getBaseForm(String word)
	{
		WordNetDatabase database = WordNetDatabase.getFileInstance();
		 
		Morphology id = Morphology.getInstance();
 
		String[] arr = id.getBaseFormCandidates(word, SynsetType.ADJECTIVE);
 
		return arr;
	}
}
