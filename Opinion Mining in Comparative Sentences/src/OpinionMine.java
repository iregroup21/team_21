import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;


public class OpinionMine {
	
	
	static ArrayList<String> entity_1 , entity_2;
	static ArrayList<String> compw , feature;
	static IsOpinionated iopn;
	static Type type;
	static Direction dir;
	static OSA osa;
	static Occurrence occ;
	static BaseForm bsf;
	static PrintWriter outputfile;
	
	static
	{
		//System.setProperty("wordnet.database.dir", "C:\\Program Files (x86)\\WordNet\\2.1\\dict");
		try {
			outputfile=new PrintWriter("output.xml");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		iopn=new IsOpinionated();
		type=new Type();
		dir=new Direction();
		osa=new OSA();
		occ=new Occurrence();
		bsf=new BaseForm();
	}
	
	public static void main(String[] arguments) throws FileNotFoundException {
		// TODO Auto-generated method stub
		
		//System.setProperty("wordnet.database.dir", arguments[0]);
		
	}
	
	public void getPreferenceFromFile(String filepath)
	{
		HashMap<String,String[]> sentences;
		Parser parser=new Parser(filepath);
		//outputfile=new PrintWriter("output.xml");
		sentences=parser.Parse();
		
		
		outputfile.println("<file>");
		for(String sentence : sentences.keySet())
		{
		outputfile.println("<record>");
		outputfile.println("<sentence>"+sentence+"</sentence>");
		
		String[] args=sentences.get(sentence);
		
	//	System.out.println("args:"+args.length);
		
		outputfile.print("<breakdown>");
		String breakdown="";
		for(String temp : args)
			breakdown+=temp+" ";
		
		outputfile.print(breakdown.trim());
		outputfile.print("</breakdown>");	
		
		
		outputfile.println();
		
		
		
		breakArgs(entity_1,entity_2,feature,compw,args);
	/*	System.out.println(entity_1);
		System.out.println(entity_2);
		System.out.println(feature);
			System.out.println(compw);  */
	
		
		boolean isSuperlative=entity_2.isEmpty();
		
	//	if(entity_2.isEmpty())outputfile.println("SUPERLATIVE");
		
		int pref=1;
		
		try{
			pref=getPreference();
		}
		catch(Exception e)
		{
			outputfile.println("Exception");
		}
		
		outputfile.println("<originaloutput>"+pref+"</originaloutput>");
	//	outputfile.println("<expectedoutput>"+"</expectedoutput>");
		
		outputfile.print("<Entityname>");
		
		if(pref==1)
		{
			
			for(String s:entity_1){
				outputfile.println(s+" ");
			}
		}
		else
		{
			if(!isSuperlative){
			for(String s:entity_2)
				outputfile.println(s+" ");
			}
			else
			{
				outputfile.println("Entity 1 not preferred");
			}
		}
		
		
		outputfile.println("</Entityname>");
		outputfile.println("</record>");
		outputfile.println("");
		
		}
		
		outputfile.println("</file>");
		outputfile.close();
	}
	
	
	public static int getPrefFromBreakdown(String BreakDown , String sentencestr) throws Exception
	{
		breakArgs(entity_1,entity_2,feature,compw,BreakDown.trim().split(" +"));
		
		int pref=1;
		
		pref=getPreference();
		
	//	System.out.println("Here:"+BreakDown + " "+pref);
		
		if(Negation.CheckNegativeComparator(sentencestr, compw.get(0)))
			pref=(pref==1?2:1);
			
		return pref;
	}
	
	
	public static void breakArgs(ArrayList<String> entity_12,
			ArrayList<String> entity_22, ArrayList<String> feature2,
			ArrayList<String> compw2, String[] args) {				//Get the entities , feature , comparator from the breakdown
		// TODO Auto-generated method stub
		
		entity_1=new ArrayList<String>();
		entity_2=new ArrayList<String>();
		feature=new ArrayList<String>();
		compw=new ArrayList<String>();
		
		int serial=0;	// which object this is 1_ or 2_ or 3_
		boolean i1,i2,i3,i4;
		
		i1=i2=i3=i4=false;
		String start="";
		for(int i=0;i<args.length;i++)
		{
			char ch=args[i].charAt(0);
			int length=args[i].length();
			
			start="";
			
			if(length>0 && args[i].charAt(0)=='(')start=args[i].substring(0,1);
			else if(length>2)start=args[i].substring(0,2);
			
			
			
			switch(start)
			{
			case "1_":	// Entity 1
						i1=i2=i3=i4=false;
						entity_1.add(args[i].substring(2));						
						i1=true;
						break;
						
			case "2_":	// Entity 2
						i1=i2=i3=i4=false; 
						i2=true;
						entity_2.add(args[i].substring(2));
						break;
						
			case "3_": 
						// Feature
						i1=i2=i3=i4=false;
						i3=true;
						feature.add(args[i].substring(2));
						break;
						
			case "(": 	i1=i2=i3=false; 
						i4=true;
						if(args[i].charAt(length-1)==')' || args[i].charAt(length-1)==',')
							compw.add(args[i].substring(1, args[i].length()-1));
						else
							compw.add(args[i].substring(1, args[i].length()));	
						break;
						
			default:    
						if(i1)entity_1.add(args[i]);
						else if(i2)entity_2.add(args[i]);
						else if(i3)feature.add(args[i]);
						else{
						if(args[i].charAt(length-1)==')')
							compw.add(args[i].substring(0, args[i].length()-1));
						else
							compw.add(args[i].substring(0, args[i].length()));
						}
						break;
			}
		}
	
		
}


	public static int getPreference() throws Exception
	{
		
		//System.out.println("word is:"+compw.get(0));
		Object[] compwoarr=compw.toArray();
		String[] compwarr = Arrays.copyOf(compwoarr, compwoarr.length , String[].class);
		int complen=compw.size();
		
		//System.out.println("compwarr length:"+compwarr.length);
		
		
		
/************************************************* For Type 1******************************************************************/

if(feature.size() ==0)
{
		/******************************************************** 1A ***********************************************************************/
		
		if(iopn.IsCWOpinionated(compw.get(complen-1)))    	// compw is list	// 1A
		{
			outputfile.println("<type>"+"1A"+"</type>");
		//	System.out.println("<type>"+"1A"+"</type>");
		//	if(type.WType(compwarr)==1) //compw.size()==1)	 // Uncomment this				
			//{
				System.out.println(compw.get(complen-1));
				if(iopn.IsPositive(compw.get(0)))
				{
					return 1;
				}
				else return 2;
			//}
		}	
	
		/******************************************************** 1B ***********************************************************************/		
		else if(compw.size()>1 && iopn.IsCWOpinionated(compw.get(complen-1)))			
	        {
			
	           String w1=compw.get(0);
	           String w2=compw.get(1);	// 
	            
	           outputfile.println("<type>"+"1B"+"</type>");
	          // System.out.println("<type>"+"1B"+"</type>");
	            if(dir.getDirection(w1)=="inc" && iopn.IsNegative(w2))
	            {
	            //	outputfile.println("Condition 1");
	                return 2;
	            }
	            else if(dir.getDirection(w1)=="inc" && iopn.IsPositive(w2))
	            {
	           // 	outputfile.println("Condition 2");
	                return 1;
	            }
	            else if(dir.getDirection(w1)=="dec" && iopn.IsNegative(w2))
	            {
	            //	outputfile.println("Condition 3");
	                return 1;
	            }
	            else if(dir.getDirection(w1)=="dec" && iopn.IsPositive(w2))
	            {
	            //	outputfile.println("Condition 4");
	                return 2;
	            }
	        }
		
	
	
		/******************************************************** 1C ***********************************************************************/
	
	if(compw.size()>1 && !iopn.IsCWOpinionated(compw.get(0))  && !iopn.IsCWOpinionated(compw.get(complen-1)))
	{
		outputfile.println("<type>"+"1C"+"</type>");
	//	System.out.println("<type>"+"1C"+"</type>");
		return osa.OSAPref(compw.get(complen-1), compw.get(0));
	}
	
	
	
		/********************************************************* 1D *************************************************************************/
	
	if(compw.size()==1)		// is feature indicator
	{
		
		outputfile.println("<type>"+"1D"+"</type>");
	//	System.out.println("<type>"+"1D"+"</type>");
		//System.out.println("<type>"+"1D"+"</type>");
		
	//	if(compw.get(0).startsWith("hea"))System.out.println(compw.get(0)+"(:)"+occ.OC_cons(compw.get(0)) + " and " + occ.OC_pros(compw.get(0)));
		
		if(occ.OC_cons(compw.get(0)) > occ.OC_pros(compw.get(0)))
			return 2;
		
		return 1;
		
	}
		
}


/**************************************************************** Type 2 *********************************************************************/

else {
	
	/*if(compw.size()==1 && iopn.IsCWOpinionated(compw.get(0))){
		if(iopn.IsNegative(compw.get(0)))return 2;
		return 1;
	}*/

	/**********************************************************2A***************************************************/
	if(compw.size()>1 && !iopn.IsCWOpinionated(compw.get(0)) && iopn.IsCWOpinionated(compw.get(complen-1))) {
		
		String w1=compw.get(0);
        String w2=compw.get(1);	// 
         
        outputfile.println("<type>"+"2A"+"</type>");
      //  System.out.println("<type>"+"2A"+"</type>");
         if(dir.getDirection(w1)=="inc" && iopn.IsNegative(w2))
         {
         	//outputfile.println("Condition 1");
             return 2;
         }
         else if(dir.getDirection(w1)=="inc" && iopn.IsPositive(w2))
         {
         	//outputfile.println("Condition 2");
             return 1;
         }
         else if(dir.getDirection(w1)=="dec" && iopn.IsNegative(w2))
         {
         //	outputfile.println("Condition 3");
             return 1;
         }
         else if(dir.getDirection(w1)=="dec" && iopn.IsPositive(w2))
         {
         	//outputfile.println("Condition 4");
             return 2;
         }
		
		
	}
	
	/**********************************************************2B***************************************************/
	
	else
	{
		
			outputfile.println("<type>"+"2B"+"</type>");
			int prefentity=-1;
		//	System.out.println("<type>"+"2B"+"</type>");
			
			String adj=compw.get(complen - 1);
			
		//	if(!dir.IsIncreasing(adj) && !dir.IsDecreasing(adj)){
			
			if(iopn.IsCWOpinionated(adj) && complen==1)
			{
				if(iopn.IsPositive(adj))return 1;
				else return 2;
				
				
			}
					//2B as (1C or 1D)
			
			
			
			//System.out.println("feature:"+feature.get(0)+" pref:"+prefentity);
			
		/*	if(compw.size() == 1 && (dir.IsIncreasing(compw.get(0)) || dir.IsDecreasing(compw.get(0))))
			{
				prefentity=(occ.OC_pros(compw.get(0))>=occ.OC_cons(compw.get(0)))?1:0;
			}
			else
			*/
		//	if(!dir.IsIncreasing(adj) && !dir.IsDecreasing(adj)){					// Changed
			prefentity=osa.OSAPref(feature.get(0), compw.get(complen-1));
			
			
			
			if(compw.size() > 1)	// If not as 1D apply 1B
			{
				
				if(dir.IsIncreasing(compw.get(0)))prefentity=(prefentity==1?1:2);
				else prefentity=(prefentity==1?2:1);
			}
		//	}
		/*	else{		// Whole else is changed . To jump back , remove if else just keep content of if
				
				boolean isPro = (occ.OC_cons(feature.get(0)) > occ.OC_pros(feature.get(0)) ? false : true);
				
				if(isPro){
					if(dir.IsIncreasing(adj))return 1;
					else return 2;
				}
				else{
					if(dir.IsIncreasing(adj))return 2;
					else return 1;
				}
			}*/
		/*}
			else{	// There is only increasing / decreasing word with feature
				
					prefentity=(occ.OC_pros(feature.get(0)) > occ.OC_cons(feature.get(0)) ? 1 : 2);
					
					if(dir.IsIncreasing(compw.get(0)))prefentity=(prefentity==1?1:2);
					else prefentity=(prefentity==1?2:1);									
			}*/
			
		
		//	if(occ.OC_cons(compw.get(0)) > occ.OC_pros(compw.get(0)))
		//		return 2;
			
		//	return 1;
			return prefentity;
		
	}
}
		return 1;
	}
}
