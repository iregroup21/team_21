import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class XMLHandler extends DefaultHandler{
	
		boolean sentence,breakdown,type,expout;
		String sentencestr , breakdownstr , typestr; 
		StringBuilder cstr; // Character stream from xml
		int count=0;
		
		int expected , total , correct , incorrect ;
		int correctpositive , allreturnedpositive ,  allpositive; 
		int non2b=0;
		
		
		public XMLHandler()
		{
			sentence=breakdown=type=expout=false;
			expected=total=correct=incorrect=0;	
			correctpositive=allreturnedpositive=allpositive=0;
			cstr=new StringBuilder();
		}
		
		
		 @Override
		 public void startElement(String uri, String localName, String qName, Attributes attributes)
		        throws SAXException {
		    	if (qName.equalsIgnoreCase("sentence")){sentence=true;cstr.setLength(0);}
			 	else if (qName.equalsIgnoreCase("breakdown")){ breakdown = true;cstr.setLength(0);} 
			 	else if (qName.equalsIgnoreCase("type") ){ type = true;cstr.setLength(0);}
			 	else if(qName.equalsIgnoreCase("expectedoutput") ){ expout = true;cstr.setLength(0);}
			 	
		    }
		 
		 
		 public void characters(char ch[], int start, int length) throws SAXException {
			 
		    	if(sentence || breakdown || expout || type)
		    	{
		    	cstr.append(ch, start, length);		    	
		    	}
		    	
		    }
		 
		 
		 
		 public void endElement(String uri, String localName, String qName)
			      	{
			    	if (qName.equalsIgnoreCase("sentence")){sentence=false;sentencestr=cstr.toString();}
				 	else if (qName.equalsIgnoreCase("breakdown")){ breakdown = false;breakdownstr=cstr.toString();} 
				 	else if (qName.equalsIgnoreCase("type") ){ type = false;typestr=cstr.toString();}
				 	else if(qName.equalsIgnoreCase("expectedoutput") )
				 	{
				 	//	System.out.println("Evaluating:"+cstr);
				 		expout = false;
				 		
				 		
				 		int original=0;
				 		try {
				 			expected=Integer.parseInt(cstr.toString());
							original=OpinionMine.getPrefFromBreakdown(breakdownstr , sentencestr);
						} catch (Exception e) {
							// TODO Auto-generated catch block
						//	System.out.println("+++"+sentencestr+"+++");
						//	e.printStackTrace();
						}
				 		
				 	//	System.out.println(count+":"+breakdownstr);
				 	//	System.out.println();
				 		count++;
				 		total++;
				 		
				 		
				 		if(original==1 && expected==1)correctpositive++;
				 		if(original==1)allreturnedpositive++;
				 		if(expected==1)allpositive++;
				 		
				 		if(original==expected)correct++;
				 		
				 	 if(expected==original && original==1){
				 	/*		System.out.println("***********************************************");
				 			System.out.println(sentencestr);
				 			System.out.println(breakdownstr);
				 			System.out.println(typestr);
				 			System.out.println("E:"+expected+" O:"+original);
				 			System.out.println("***********************************************");
				 			System.out.println();*/
				 			
				 			incorrect++;
				 		}
				 		
				 		
				 	}
				 }
			    	

}
