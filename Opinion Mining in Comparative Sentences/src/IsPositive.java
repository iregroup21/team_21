/* takes a command line argument word and says is its a positive word or not */
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
 
public class IsPositive {
 
	public static void main(String[] args) {
            String word="";
            for (String s: args) {
                word+=s;
            }
 
		BufferedReader br = null;
                int flag =0;
 
		try {
 
			String sCurrentLine;
 
			br = new BufferedReader(new FileReader("positive-words.txt"));
 
			while ((sCurrentLine = br.readLine()) != null) {
                                if(sCurrentLine.equals(word)==true){
                                    System.out.println("its positive");
                                    flag=1;
                                }
//				System.out.println(sCurrentLine.getClass().getSimpleName());
			}
                        if(flag==0)
                            System.out.println("can't guarantee");
 
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
 
	}
}
