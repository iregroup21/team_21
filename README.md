# README #


### Opinion Mining in Comparative Sentences ###

* Quick summary : This repository contains the project "Opinion mining from Comparative sentences". This project focuses at mining the user opinions about various kind of products from the reviews they provide at various websites. The major focus is upon the comparative sentences (Sentences in which 2 entities/products are compared) although it also covers superlative sentences. 

*Application : This project can be very useful for various product companies to evaluate their products based on their comparisons from other similar products using millions of feedback statements present on web in an Automated way without human intervention.

* [Learn Markdown] :

  1.   Project Demonstration : [Youtube](https://youtu.be/1Pgp6vw7fPs)
  2.   Project Presentation  : [Slideshare](http://www.slideshare.net/RashmiPatange/mining-opinions-from-comparative-sentences-ireteam21)

### How do I get set up? ###

* Summary of set up :
      1. Clone the repository.
      2. Import using an IDE (preferably eclipse)
      3. Install Wordnet in your system (You can find it [here](http://wordnetcode.princeton.edu/tools/))      
      4. Open the repo in terminal.
      5. run as "java -jar OMfCS.jar "<path to dict folder of wordnet (usually C:\\Program Files (x86)\\WordNet\\2.1\\dict)>"

* Dependencies : Wordnet